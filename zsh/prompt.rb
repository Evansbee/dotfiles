require 'open3'


COLORS = ['%{$fg[blue]%}',
   '%{$fg[yellow]%}',
   '%{$fg[magenta]%}',
   '%{$fg[white]%}',
   '%{$fg[green]%}',
   '%{$fg[red]%}',
   '%{$fg[cyan]%}']

RESET = '%{$reset_color%}'

def colorize(text)
   if text
      text = ""+text+""
      COLORS[text.to_i(36)%COLORS.length] + text + RESET
   else 
      ""
   end
end

def git_string
   stdin,stdout, stderr = Open3.popen3('git status -bs')
   if stderr.readlines.count  > 0
      ""
   else
      stdout = stdout.readlines
      git = " <" + colorize(stdout[0].split('.')[0].split[1]) + ">"
      git += " " + COLORS[5] + "!" + RESET if stdout.count > 1
      git
   end
end

path = Dir.pwd

output_string = (path.split('/')[0...-1].map{|x| colorize(x[0])} + [path.split('/')[-1]]).join('/') + git_string

print output_string
