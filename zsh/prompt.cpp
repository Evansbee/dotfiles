#include <unistd.h>
#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
#include <sstream>

#include <fstream>
#include <git2.h>


static const char *colors[] = {
   "%{$fg[blue]%}",
   "%{$fg[yellow]%}",
   "%{$fg[magenta]%}",
   "%{$fg[white]%}",
   "%{$fg[green]%}",
   "%{$fg[red]%}",
   "%{$fg[cyan]%}"};

static short NUM_COLORS = 7;


static const char *reset = "%{$reset_color%}";



std::vector<std::string> &split(const std::string& s, char delim, std::vector<std::string> &elems)
{
   std::stringstream ss(s);
   std::string item;

   while(getline(ss, item, delim))
   {
      elems.push_back(item);
   }
   return elems;
}

std::vector<std::string> split(const std::string &s, char delim)
{
   std::vector<std::string> elems;
   split(s,delim,elems);
   return elems;
}

std::string make_color(const std::string in, bool take_all_chars = true)
{
   //if 0 then take the whole string
   std::string working = std::string(colors[in.c_str()[0] % NUM_COLORS]);
   if(take_all_chars)
   {
      working = working + in;
   }
   else
   {
      working = working + in[0];
   }

   working = working + std::string(reset);
   return working;
}


int main()
{
   char cwd[1024];
   getcwd(cwd,sizeof(cwd));
   auto dir = std::string(cwd);

   auto path_elements = split(dir, '/');

   std::string prompt = "";

   for(auto i = 1; i < path_elements.size(); ++i)
   {
      prompt += "/"; prompt +=  make_color(path_elements[i],i == (path_elements.size() - 1)); 
   }

   git_libgit2_init();

   git_repository *repo = NULL;
   git_buf root = {0};
   int error = git_repository_discover(&root, cwd, 0, NULL);

   if(error == 0)
   {
      std::string repository = std::string(root.ptr);
      git_buf_free(&root);
      std::string head_info = repository + "/HEAD";
      auto f = std::ifstream(head_info, std::ifstream::in);
  
      std::string line;
      getline(f,line);

      f.close();

      auto parts = split(line, '/');

      prompt = "[" + make_color(parts[parts.size()-1]) +"] " + prompt;

   }

   std::cout<<prompt;

   return 0;
}



