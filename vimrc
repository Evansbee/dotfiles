autocmd!

set nocompatible
set hidden
set history=10000
set expandtab
set tabstop=3
set shiftwidth=3
set softtabstop=3
set autoindent
set laststatus=2
set showmatch
set incsearch
set hlsearch
set ignorecase smartcase
set cursorline
set cmdheight=1
set switchbuf=useopen
set showtabline=2
set winwidth=79
set shell=bash
set scrolloff=3
set nobackup
set nowritebackup
set backspace=indent,eol,start
set showcmd
syntax on
filetype plugin indent on
set wildmode=longest,list
set wildmenu
let mapleader=","
inoremap jj <Esc>
set timeout timeoutlen=1000 ttimeoutlen=100
set modeline
set modelines=3
set foldmethod=manual
set nofoldenable
set nojoinspaces
set autoread
set number
set numberwidth=5
set t_Co=256
hi CursorLine term=bold cterm=bold guibg=Grey30 ctermbg=240
command WQ wq
command Wq wq
command W w
command Q q
nnoremap WQ ggVG=<CR>:wq<CR>
map <C-s> <esc>:w<CR>
imap <C-s> <esc>:w<CR>
