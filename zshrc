
if which rbenv > /dev/null; then eval "$(rbenv init -)"; fi

autoload -U compinit promptinit
autoload -U colors && colors

setopt prompt_subst

compinit
promptinit

zstyle ':completion:*' menu select

alias source='source ~/.zshrc'

export PATH="$PATH:$HOME/.rvm/bin"


export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8

function precmd {
local RG="%(?,%{$fg[green]%},%{$fg[red]%})"
local KL="%{$reset_color%}"
PROMPT=" ${RG}❰${KL} $(~/.zsh/prompt) ${RG}❱${KL} "
}

precmd
alias ls='ls -G'


stty start undef
stty stop undef

alias gc='git commit'
alias ga='git add'
alias gco='git checkout'

